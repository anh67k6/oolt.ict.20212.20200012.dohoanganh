package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable  {
	
	public DigitalVideoDisc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	
	public DigitalVideoDisc(String title, String category, float cost,String director, int length) {
		super(title, category, cost);
		this.director= director;
		this.length= length;
	}

	
	private String director;
	private int length;

    public boolean search(String searchTitle) {
        String[] stringArray = title.split(" ");
        String[] searchTitleArray = searchTitle.split(" ");
        boolean[] check = new boolean[searchTitleArray.length];
        
        for (int i = 0; i < searchTitleArray.length; ++i) {
            check[i] = false;
            for (String t : stringArray) {
                if (t.toLowerCase().contains(searchTitleArray[i].toLowerCase())) {
                    check[i] = true;
                    break;
                }
            }
        }

        boolean finalCheck = true;
        for (int i = 0; i < searchTitleArray.length; ++i) {
            if (check[i] == false) {
                finalCheck = false;
            }
        }

        return finalCheck;
    }
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}
