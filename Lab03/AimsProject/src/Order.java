import java.util.ArrayList;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private ArrayList<DigitalVideoDisc> itemOrdered = new ArrayList<DigitalVideoDisc>();

	public int qtyOrdered() {
		return itemOrdered.size();
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(itemOrdered.size() > MAX_NUMBERS_ORDERED) {
			System.out.println("Failed. Maximum number!!!");
		} else {
			itemOrdered.add(disc);
			
			System.out.println("Add book " + itemOrdered.size() + " sucess!!");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index = -1;
		
		for(int i=0;i<itemOrdered.size();i++) {
			if(disc.equals(itemOrdered.get(i))){
				index = i;
			}
		}
		
		if(index == -1) {
			System.out.println("Cant find ^^");
		} else {
			System.out.println("Delete done");
			itemOrdered.remove(index);
		}
	}
	
	public float totalCost() {
		float sum = 0;		
		for(int i=0; i<itemOrdered.size();i++) {
			sum+= itemOrdered.get(i).getCost();
		}
		return sum;
	}
}
