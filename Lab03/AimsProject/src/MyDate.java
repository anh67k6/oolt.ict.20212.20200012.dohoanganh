import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public MyDate() {
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		this.month = localDate.getMonthValue();
		this.year = localDate.getYear();
		this.day = localDate.getDayOfMonth();
	}
	
	public MyDate(int day, int month, int year ) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		LocalDate current_date = LocalDate.parse(date, formatter);
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	
	public MyDate accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a date:");
		String date_str =  sc.nextLine();
		MyDate date = new MyDate(date_str);
		return date;
	}
	
	public void print() {
		System.out.println(day+", "+month+", "+year); 
	}
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if(day>=1 && day <=31) {
			this.day = day;
		}else
			System.out.println("Invalid day");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else
			System.out.println("Invalid month");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}
