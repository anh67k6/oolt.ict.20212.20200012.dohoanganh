package hust.soict.globalict.aims.disc;

public class DigitalVideoDisc {
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	
    public boolean search(String searchTitle) {
        String[] stringArray = title.split(" ");
        String[] searchTitleArray = searchTitle.split(" ");
        boolean[] check = new boolean[searchTitleArray.length];
        
        for (int i = 0; i < searchTitleArray.length; ++i) {
            check[i] = false;
            for (String t : stringArray) {
                if (t.toLowerCase().contains(searchTitleArray[i].toLowerCase())) {
                    check[i] = true;
                    break;
                }
            }
        }

        boolean finalCheck = true;
        for (int i = 0; i < searchTitleArray.length; ++i) {
            if (check[i] == false) {
                finalCheck = false;
            }
        }

        return finalCheck;
    }
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
}
