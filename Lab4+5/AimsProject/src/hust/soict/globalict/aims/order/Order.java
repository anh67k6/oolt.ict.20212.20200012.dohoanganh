package hust.soict.globalict.aims.order;
import java.util.ArrayList;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private MyDate dateOrdered;
	private ArrayList<DigitalVideoDisc> itemOrdered = new ArrayList<DigitalVideoDisc>();
		
	public Order() {
		MyDate date = new MyDate();
		this.dateOrdered = date;
		
		if(nbOrders<MAX_LIMITTED_ORDERS)
			nbOrders++;
		else 
			System.out.println("You can not add any order due to limitation reacched");
		
	}
	public int qtyOrdered() {
		return itemOrdered.size();
	}

	//Add one item
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(itemOrdered.size() > MAX_NUMBERS_ORDERED) {
			System.out.println("Failed. Maximum number!!!");
		} else {
			itemOrdered.add(disc);
			
			System.out.println("Add book " + itemOrdered.size() + " sucess!!");
		}
	}
	
	//Add a lot of item
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		int i=0;
		while(itemOrdered.size() < MAX_NUMBERS_ORDERED) {
			itemOrdered.add(dvdList[i]);
			System.out.println("Add book "+ itemOrdered.size()+ " sucess!");
			i = i+1;
		}
		for(int j=i; j<dvdList.length;j++) {
			System.out.println("Cant add "+ dvdList[j].getTitle() + ". Full !!!");
		}
	}
	
	//Add two item
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {		
		if(itemOrdered.size() <= MAX_NUMBERS_ORDERED - 2) {
			itemOrdered.add(dvd1);
			itemOrdered.add(dvd2);
		} else if (itemOrdered.size() <= MAX_NUMBERS_ORDERED - 1) {
			itemOrdered.add(dvd1);
			System.out.println("Cant not add dvd" + dvd2.getTitle());
		} else {
			System.out.println("Cant not add dvd" + dvd1.getTitle());
			System.out.println("Cant not add dvd" + dvd2.getTitle());
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index = -1;
		
		for(int i=0;i<itemOrdered.size();i++) {
			if(disc.equals(itemOrdered.get(i))){
				index = i;
			}
		}
		
		if(index == -1) {
			System.out.println("Cant find ^^");
		} else {
			System.out.println("Delete done");
			itemOrdered.remove(index);
		}
	}
	
	public float totalCost() {
		float sum = 0;		
		for(int i=0; i<itemOrdered.size();i++) {
			sum+= itemOrdered.get(i).getCost();
		}
		return sum;
	}
	
	public void printOrdered() {
		System.out.println("*************************Order*************************************************************");
		System.out.println("Date[" +dateOrdered.printDate()+ "]");
		System.out.println("Ordered Items:");
		for (int i=0;i<itemOrdered.size();i++) {
			System.out.println((i+1) + ". ["+ itemOrdered.get(i).getTitle()+"] - ["+itemOrdered.get(i).getCategory()+"] - ["+itemOrdered.get(i).getDirector()+"] - ["+itemOrdered.get(i).getLength()+"] - ["+itemOrdered.get(i).getCost()+"] $");
		}
		System.out.println("*******************************************************************************************");
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int rand = (int)(Math.random()*qtyOrdered());
		
		DigitalVideoDisc luckyDisc = itemOrdered.get(rand);
		
        for(DigitalVideoDisc i : itemOrdered) {
            if (i.equals(luckyDisc)) {
                i.setCost(0);
            }
        }
		return luckyDisc;
	}
}
