package hust.soict.globalict.aims;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MyDate;

public class Aims {
	public static void main(String[] args) {
		Order anOrder = new Order();
		MyDate date = new MyDate();
		date.print();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setDirector("RG");
		dvd1.setLength(87);
		dvd1.setCost(19.95f);
		//Add dvd 1 to list
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War");
		dvd2.setCategory("Science Fiction");
		dvd2.setDirector("GL");
		dvd2.setLength(90);
		dvd2.setCost(24.95f);
		//Add dvd 2 to list
		anOrder.addDigitalVideoDisc(dvd2);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setDirector("JS");
		dvd3.setLength(90);
		dvd3.setCost(18.99f);
		//Add dvd 3 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 4 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 5 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 6 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 7 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 8 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		//Add dvd 9 to list
		anOrder.addDigitalVideoDisc(dvd3);
		
		
		DigitalVideoDisc arrList[] =  {dvd3, dvd3, dvd3};
		anOrder.addDigitalVideoDisc(arrList);
		System.out.println("Total cost is: "+anOrder.totalCost());
		System.out.println("Quantity books: "+ anOrder.qtyOrdered());
//		anOrder.removeDigitalVideoDisc(dvd3);
//		System.out.println("Quantity books: "+ anOrder.qtyOrdered());
//		anOrder.removeDigitalVideoDisc(dvd3);
		
//		anOrder.orderDate.print();
		
		anOrder.printOrdered();
		
		anOrder.getALuckyItem();
	}
}
