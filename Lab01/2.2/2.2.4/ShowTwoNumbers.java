import javax.swing.JOptionPane;

public class ShowTwoNumbers {
    public static void main(String[] args) {
        String strNumb1, strNumb2;
        String strNoti = "You've just entered ";

        strNumb1 = JOptionPane.showInputDialog(null, "Input the first number: ", JOptionPane.INFORMATION_MESSAGE);
        strNoti += strNumb1 + " and ";

        strNumb2 = JOptionPane.showInputDialog(null, "Input the second number: ", JOptionPane.INFORMATION_MESSAGE);
        strNoti += strNumb2;

        JOptionPane.showMessageDialog(null, strNoti, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
