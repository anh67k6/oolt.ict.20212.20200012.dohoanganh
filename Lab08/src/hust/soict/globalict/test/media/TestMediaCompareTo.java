package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		
		do {
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Test the compareTo() in DVD");
			System.out.println("2. Test the compareTo() in Track");
			System.out.println("3. Test the compareTo() in CD");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3");
			
			choice = sc.nextInt();
			
			switch(choice) {
			case 1:
				List<DigitalVideoDisc> dvddiscs = new ArrayList<DigitalVideoDisc>();
				DigitalVideoDisc dvd1= new DigitalVideoDisc("The Lion King","Animation",19.95f,"Roger Allers",87);
				dvd1.play();
				DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars","Science Fiction",24.95f,"George Lucas",124);
				dvd2.play();
				DigitalVideoDisc dvd3= new DigitalVideoDisc("Aladdin","Animation",18.99f,"John Musker",90);
				dvd3.play();
				
				
				//Add the DVD objects to the ArrayList
				dvddiscs.add(dvd2);
				dvddiscs.add(dvd1);
				dvddiscs.add(dvd3);
				
				//Iterate through the Arraylist and output their titles
				// unsorted order
				Iterator iterator= dvddiscs.iterator();
				System.out.println("---------------------------------------------");
				System.out.println("The DVDs currently in the order are:");
				while(iterator.hasNext()) {
					System.out.println(((Media) iterator.next()).getTitle());
				}
				
				//Sort the collection of DVDs-based on the compareTo()
				//method
				Collections.sort(dvddiscs);
				//iterate through the Array list and output their titles
				//in sorted order
				iterator=dvddiscs.iterator();
				
				System.out.println("---------------------------------------------");
				System.out.println("The DVDs in the sorted order are:");
				while(iterator.hasNext()) {
					System.out.println(((Media) iterator.next()).getTitle());
				}
				
				System.out.println("---------------------------------------------");
				break;
			case 2:
				List<Track> tracks = new ArrayList<Track>();
				Track track1 = new Track("There is no one at all",500);
				track1.play();
				Track track2 = new Track("Xa em",400);
				track2.play();
				Track track3 = new Track("Dieu anh biet",200);
				track3.play();
				//Add the DVD objects to the ArrayList
				tracks.add(track1);
				tracks.add(track2);
				tracks.add(track3);
				
				//Iterate through the Arraylist and output their titles
				// unsorted order
				Iterator iterator1= tracks.iterator();
				System.out.println("---------------------------------------------");
				System.out.println("The Track currently in the order are:");
				while(iterator1.hasNext()) {
					System.out.println(((Track) iterator1.next()).getTitle());
				}
				
				//Sort the collection of DVDs-based on the compareTo()
				//method
				Collections.sort(tracks);
				//iterate through the Array list and output their titles
				//in sorted order
				iterator1=tracks.iterator();
				
				System.out.println("---------------------------------------------");
				System.out.println("The Track in the sorted order are:");
				while(iterator1.hasNext()) {
					System.out.println(((Track) iterator1.next()).getTitle());
				}
				
				break;
			case 3:
				List<CompactDisc> cddiscs = new ArrayList<CompactDisc>();
				CompactDisc cd1= new CompactDisc("25","Ballad",19.95f,"Hoang Dung");
				cd1.addTrack(new Track("Nang tho",500));
				cd1.addTrack(new Track("Thoi quen",100));
				cd1.play();
				CompactDisc cd2= new CompactDisc("Dong","Ballad",24.95f,"Noo Phuoc Thinh");
				cd2.addTrack(new Track("Xa em",400));
				cd2.addTrack(new Track("Mua dong",700));
				cd2.play();
				CompactDisc cd3= new CompactDisc("Anh yeu em","Ballad",18.99f,"Hoang Anh");
				cd3.addTrack(new Track("Anh ghet em",200));
				cd3.addTrack(new Track("Anh ghet em 2",300));
				cd3.play();
				
				
				//Add the CD objects to the ArrayList
				cddiscs.add(cd1);
				cddiscs.add(cd2);
				cddiscs.add(cd3);
				
				//Iterate through the Arraylist and output their titles
				// unsorted order
				Iterator iterator2= cddiscs.iterator();
				System.out.println("---------------------------------------------");
				System.out.println("The Cds currently in the order are:");
				while(iterator2.hasNext()) {
					System.out.println(((Media) iterator2.next()).getTitle());
				}
				
				//Sort the collection of CDs-based on the compareTo()
				//method
				Collections.sort((List)cddiscs);
				//iterate through the Array list and output their titles
				//in sorted order
				iterator2=cddiscs.iterator();
				
				System.out.println("---------------------------------------------");
				System.out.println("The CDs in the sorted order are:");
				while(iterator2.hasNext()) {
					System.out.println(((Media) iterator2.next()).getTitle());
				}
				
				System.out.println("---------------------------------------------");
				break;
			case 0:
			System.exit(0);
			return;

			}
		} while (choice !=0);
		
	}
}
