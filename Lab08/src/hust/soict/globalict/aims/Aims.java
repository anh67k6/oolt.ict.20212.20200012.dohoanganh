package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;
import hust.soict.globalict.aims.utils.MyDate;

public class Aims {
	public static void main(String[] args) {
		MemoryDaemon memo= new MemoryDaemon();
		showMenu();
	}
	
	public static void showMenu() {

		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		do{
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
		switch (choice){
			case 1:
				Order newOrder = new Order();
				userOrder.add(newOrder);
				System.out.println("There's a new order with the id: " + newOrder.id);
				break;
			case 2:
				System.out.println("Input the id of the order you want to add items to: ");
				sc = new Scanner(System.in);
				int inputId = sc.nextInt();
				int search = orderSearch(inputId, userOrder);
				
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the number of the items you want to add: ");

				int inputNum = sc.nextInt();
				
				for (int i = 0; i < inputNum; i++) {
					System.out.println("Which type do you want to add: Book, CompactDisc or DigitalVideoDisc?");
					String option = sc.next();
					if(option.equals("Book")) {
						System.out.println("Enter title: ");
						String title = sc.next();
						System.out.println("Enter category: ");
						String category = sc.next();
						System.out.println("Enter cost: ");
						float cost = sc.nextFloat();
						System.out.println("Enter number of authors: ");
						int numOfAuthors = sc.nextInt();
						Book newBook= new Book(title,category,cost);
						for (int j=0;j<numOfAuthors;++j) {
							System.out.println("Enter author's name: ");
							String authorName = sc.next();
							newBook.addAuthor(authorName);
						}
						//add book to the order here 
						userOrder.get(search).addMedia(newBook);
					} else if (option.equals("CompactDisc")) {
						System.out.println("Enter titile: ");
						String title = sc.next();
						System.out.println("Enter category: ");
						String category = sc.next();
						System.out.println("Enter cost: ");
						float cost = sc.nextFloat();
						System.out.println("Enter the artist:");
						String artistName = sc.next();
						CompactDisc cd= new CompactDisc(title,category,cost,artistName);
						
						System.out.println("Enter the number of tracks");
						int numOfTracks = sc.nextInt();
						for (int j=0;j<numOfTracks;++j) {
							System.out.println("Enter the track's title:");
							String trackTitle = sc.next();	
							System.out.println("Enter the length");
							int trackLength = sc.nextInt();
							cd.addTrack(new Track(trackTitle,trackLength));
						}
						System.out.println("Do you want to play? 1.Yes 2.No " );
						int res= sc.nextInt();
						if(res ==1) {
							cd.play();
						} 
						
						userOrder.get(search).addMedia(cd);
					} else {
						System.out.println("Enter title: ");
						String title = sc.next();
						System.out.println("Enter category: ");
						String category = sc.next();
						System.out.println("Enter cost: ");
						float cost = sc.nextFloat();
						System.out.println("Enter the director:");
						String director = sc.next();
						System.out.println("Enter the length");
						int length = sc.nextInt();
						DigitalVideoDisc dvd= new DigitalVideoDisc(title,category,cost,director,length);
						
						//write play option code here
						System.out.println("Do you want to play? 1.Yes 2.No " );
						int res= sc.nextInt();
						if(res ==1) {
							dvd.play();
						}
						userOrder.get(search).addMedia(dvd);
					}
				}
				break;
		

			case 3:
				System.out.println("Input the id of the order you want to delete item from: ");
				int orderInputId = sc.nextInt();
				int orderSearch = orderSearch(orderInputId, userOrder);
				
				if(orderSearch == -1) {
					System.out.println("Wrong id!!");
					break;
				} 

				System.out.println("Input the id of the item you want to delete: ");
				int itemId = sc.nextInt();
				userOrder.get(orderSearch).removeMedia(itemId);
				break;

			case 4:
				System.out.println("Input the id of the order you want to display: ");
				inputId = sc.nextInt();
				search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}
				userOrder.get(search).orderPrint();
				break;
		

			case 0:
			System.exit(0);
			return;

		}
		}while (choice != 0);	
	}

	public static int orderSearch(int id, ArrayList<Order> userOrder){
		for (int i = 0; i < userOrder.size(); i++) {
            if (userOrder.get(i).id == id)
                return i;
        }
		return -1;
	}
}
