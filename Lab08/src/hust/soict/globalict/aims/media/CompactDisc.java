package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable {
	
	public CompactDisc() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CompactDisc(String title, String category, float cost, String artist) {
		super(title, category, cost);
		this.artist = artist;
		// TODO Auto-generated constructor stub
	}

	private String artist;
	private ArrayList<Track> tracks=new ArrayList<Track>();
	
	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track track) {
		int check = 0;
		for(int i=0;i<tracks.size();i++) {
			if(track.getTitle().equals(tracks.get(i).getTitle())) {
				check ++;
			}
		}
		
		if(check == 0) {
			tracks.add(track);
			System.out.println("Added success~");
		} else {
			System.out.println("Already in the list");
		}
	}
	
	public void removeTrack(Track track) {
		int check = 0;
		for(int i=0;i<tracks.size();i++) {
			if(track.getTitle().equals(tracks.get(i).getTitle())) {
				check ++;
			}
		}
		
		if(check > 0) {
			tracks.remove(track);
			System.out.println("Your track has been removed");
		} else {
			System.out.println("No track founded");
		}		
	}
	
	public int getLength() {
		int length=0;
		for (int i=0;i<tracks.size();++i) {
			length+= tracks.get(i).getLength();
		}
		return length;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		for(int i=0;i<tracks.size();++i) {
			tracks.get(i).play();
		}
	}

	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null) {
            return -1;
        }

        if (!(obj instanceof CompactDisc)) {
            return -1;
        }
        final CompactDisc cd = (CompactDisc) obj;
        if (this.tracks.size() != cd.tracks.size()) {
            return this.tracks.size()>cd.tracks.size() ? 1 : -1;
        }else {
        	if (this.getLength() != cd.getLength()) {
            	return this.getLength() > cd.getLength() ? 1 : -1;
            }
        }
		return 0;
	}
	
}
