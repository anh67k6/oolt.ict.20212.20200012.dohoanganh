package hust.soict.globalict.aims.media;

public abstract class Media {
	public int id;
    protected String title;
    private String category;
    private float cost;

    public Media() {

	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}
	
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Media)) {
            return false;
        }

        final Media media = (Media) obj;
        if (this.id != media.id) {
            return false;
        }

        return true;
    }

}
