package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Book extends Media implements Comparable {

    private List<String> authors = new ArrayList<String>();
    private String content;
    private List<String> contentTokens = new ArrayList<String>();
    private Map<String,Integer> wordFrequency = new HashMap<String, Integer>();
    
    public Book(String title, String category,float cost){
        super(title, category,cost);
    }

    public void addAuthor(String authorName){
        int check = 0;
        for (String i : authors)
            if (i.equals(authorName))
                check++;
        if (check == 0){
            authors.add(authorName);
            System.out.println("Author added successfully!");
        }
        else
            System.out.println("The author is already in the list.");
    }

    public void removeAuthor(String authorName){
        int check = -1;
        for (int i = 0; i < authors.size(); i++) {
            if (authors.get(i).equals(authorName))
                check = i;
        }
        if (check == -1)
            System.out.println("The author is not in the list.");
        else{
            authors.remove(check);
            System.out.println("Author removed successfully!");
        }
    }
    
    public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public void processContent() {
		String[] arr = content.split("[^a-zA-Z]+");
		for(int i = 0; i < arr.length; ++i ) contentTokens.add(arr[i]);
		Collections.sort(contentTokens);
		Set<String> differentTokensOnly= new HashSet<String>();
		for(String i:contentTokens)differentTokensOnly.add(i);
		for(String i:differentTokensOnly){
			int cnt=0;
			for(String j:contentTokens) {
				if(j.equals(i))
					cnt++;
			}
			wordFrequency.put(i, cnt);
		}
    }

	@Override
	public String toString() {
		return "Book's detail\n\n"+
				"-----------------------------------------\n\n"+
				"Title: "+ this.getTitle()+"\n\n"+
				"Category: "+this.getCategory()+"\n\n"+
				"Cost: "+this.getCost()+"\n\n"+
				"Author: "+ authors +"\n\n"+
				"Content: "+ content+ "\n\n"+
				"All the tokens: "+ contentTokens +"\n\n"+
				"Frequency of each distinguish token: "+ wordFrequency +"\n\n";
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
