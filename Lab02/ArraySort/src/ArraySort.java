import java.util.Arrays;

public class ArraySort {
	public static void main(String[] args) {
		int[] arr = new int[]{1,9,5,7};
		Arrays.sort(arr);
		System.out.printf("Modified arr[] : %s", Arrays.toString(arr));
		System.out.println("");
		
		int sum = 0;
		for(int i=0;i<arr.length;i++) {
			sum += arr[i];
		}
		
		System.out.println("Average is: " + (sum / arr.length));
	}

}
