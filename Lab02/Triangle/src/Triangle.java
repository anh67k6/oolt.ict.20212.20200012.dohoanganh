import java.util.Scanner;
public class Triangle {
	public static void main(String[] args) {
		Scanner number = new Scanner(System.in);
		System.out.println("Enter n: ");
		int n = number.nextInt();
		
		for (int i=0; i<n; i++) {
            for (int j=n-i; j>1; j--) {
                System.out.print(" ");
            }
            for (int j=0; j<=i; j++ ) {
                System.out.print("* ");
            }
            System.out.println();
        }
		System.exit(0);
	}
}
