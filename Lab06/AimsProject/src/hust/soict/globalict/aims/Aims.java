package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MyDate;

public class Aims {
	public static void main(String[] args) {
//		Order anOrder = new Order();
//		MyDate date = new MyDate();
//		date.print();
//		
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//		dvd1.setCategory("Animation");
//		dvd1.setDirector("RG");
//		dvd1.setLength(87);
//		dvd1.setCost(19.95f);
//		//Add dvd 1 to list
//		anOrder.addDigitalVideoDisc(dvd1);
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War");
//		dvd2.setCategory("Science Fiction");
//		dvd2.setDirector("GL");
//		dvd2.setLength(90);
//		dvd2.setCost(24.95f);
//		//Add dvd 2 to list
//		anOrder.addDigitalVideoDisc(dvd2);
//
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//		dvd3.setCategory("Animation");
//		dvd3.setDirector("JS");
//		dvd3.setLength(90);
//		dvd3.setCost(18.99f);
//		//Add dvd 3 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 4 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 5 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 6 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 7 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 8 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		//Add dvd 9 to list
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		
//		DigitalVideoDisc arrList[] =  {dvd3, dvd3, dvd3};
//		anOrder.addDigitalVideoDisc(arrList);
//		System.out.println("Total cost is: "+anOrder.totalCost());
//		System.out.println("Quantity books: "+ anOrder.qtyOrdered());
////		anOrder.removeDigitalVideoDisc(dvd3);
////		System.out.println("Quantity books: "+ anOrder.qtyOrdered());
////		anOrder.removeDigitalVideoDisc(dvd3);
//		
////		anOrder.orderDate.print();
//		
//		anOrder.printOrdered();
//		
//		anOrder.getALuckyItem();
		
		showMenu();
	}
	public static void showMenu() {

		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		do{
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
		switch (choice){
			case 1:
				Order newOrder = new Order();
				userOrder.add(newOrder);
				System.out.println("There's a new order with the id: " + newOrder.id);
				break;
			case 2:
				System.out.println("Input the id of the order you want to add items to: ");
				sc = new Scanner(System.in);
				int inputId = sc.nextInt();
				int search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the number of the items you want to add: ");
				sc = new Scanner(System.in);
				int inputNum = sc.nextInt();
				for (int i = 0; i < inputNum; i++) {
					Media newMedia = new Media();
					System.out.println("Enter title: ");
					sc = new Scanner(System.in);
					String title = sc.nextLine();
					System.out.println("Enter category: ");
					sc = new Scanner(System.in);
					String category = sc.nextLine();
					System.out.println("Enter cost: ");
					sc = new Scanner(System.in);
					float cost = sc.nextFloat();
					newMedia.setTitle(title);
					newMedia.setCategory(category);
					newMedia.setCost(cost);
					userOrder.get(search).addMedia(newMedia);
				}
				break;
		

			case 3:
				System.out.println("Input the id of the order you want to delete item from: ");
				sc = new Scanner(System.in);
				int orderInputId = sc.nextInt();
				int orderSearch = orderSearch(orderInputId, userOrder);
				if(orderSearch == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the id of the item you want to delete: ");
				sc = new Scanner(System.in);
				int itemId = sc.nextInt();
				userOrder.get(orderSearch).removeMedia(itemId);
				break;

		

			case 4:
				System.out.println("Input the id of the order you want to display: ");
				sc = new Scanner(System.in);
				inputId = sc.nextInt();
				search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}
				userOrder.get(search).orderPrint();
				break;
		

			case 0:
			System.exit(0);
			return;

		}
		}while (choice != 0);	
	}

	public static int orderSearch(int id, ArrayList<Order> userOrder){
		for (int i = 0; i < userOrder.size(); i++) {
            if (userOrder.get(i).id == id)
                return i;
        }
		return -1;
	}
}
