package hust.soict.globalict.aims.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class MyDate {
	private int day;
	private int month;
	private int year;	
	
	public MyDate() {
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		this.month = localDate.getMonthValue();
		this.year = localDate.getYear();
		this.day = localDate.getDayOfMonth();
	}
	
	public MyDate(int day, String month, int year) {
		this.day = day;
		this.month = checkMonth(month);
		this.year = year;
	}
	
	private int checkMonth(String month) {
		switch(month) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "June":
			return 6;
		case "July":
			return 7;
		case "August":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}
		return 0;
	}
	
	public MyDate(int day, int month, int year ) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		LocalDate current_date = LocalDate.parse(date, formatter);
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	
	public MyDate accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a date:");
		String date_str =  sc.nextLine();
		MyDate date = new MyDate(date_str);		
		sc.close();
		return date;
	}
	
	
	public void print() {
		//Insert the prefix after the date
		switch(this.day) {
		case 1:
			System.out.println(this.month+", "+ this.day+ "st"+", "+ this.year);
			break;
		case 2:
			System.out.println(this.month+", "+ this.day+ "nd"+", "+ this.year);
			break;
		case 3:
			System.out.println(this.month+", "+ this.day+ "rd"+", "+ this.year);
			break;
		default:
			System.out.println(this.month+", "+ this.day+ "th"+", "+ this.year);
			break;			
		}
	}
	public void printAnotherFormat() {
		String string = this.day+"/"+this.month+"/"+this.year;
		  
		Date date1;
		try {
			date1 = new SimpleDateFormat("dd/MM/YYYY").parse(string);
			SimpleDateFormat DateFor = new SimpleDateFormat("dd-MMM-yyyy");
			String stringDate = DateFor.format(date1);
			System.out.println(stringDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public String printDate() {
		return day+"/"+month+"/"+year; 
	}
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if(day>=1 && day <=31) {
			this.day = day;
		}else
			System.out.println("Invalid day");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else
			System.out.println("Invalid month");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}
