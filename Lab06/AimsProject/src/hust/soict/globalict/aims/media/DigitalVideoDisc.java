package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Media {
	private String director;
	private int length;
	
	public DigitalVideoDisc(){
		
	}

	public DigitalVideoDisc(String title){
        super(title);
    }

    public DigitalVideoDisc(String title, String category){
        super(title, category);
    }
    public boolean search(String searchTitle) {
        String[] stringArray = title.split(" ");
        String[] searchTitleArray = searchTitle.split(" ");
        boolean[] check = new boolean[searchTitleArray.length];
        
        for (int i = 0; i < searchTitleArray.length; ++i) {
            check[i] = false;
            for (String t : stringArray) {
                if (t.toLowerCase().contains(searchTitleArray[i].toLowerCase())) {
                    check[i] = true;
                    break;
                }
            }
        }

        boolean finalCheck = true;
        for (int i = 0; i < searchTitleArray.length; ++i) {
            if (check[i] == false) {
                finalCheck = false;
            }
        }

        return finalCheck;
    }
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}
